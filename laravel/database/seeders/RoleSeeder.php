<?php

namespace Database\Seeders;

use App\Enums\Roles;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::insert([
            [
                'name' => Roles::OPERATOR->value,
                'guard_name' => 'user'
            ],
            [
                'name' => Roles::USER->value,
                'guard_name' => 'user'
            ],
        ]);
    }
}
