<?php

namespace Database\Seeders;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = User::create([
            'username' => config('admin_credentials.USERNAME'),
            'email' => config('admin_credentials.EMAIL'),
            'password' => Hash::make(config('admin_credentials.PASSWORD')),
            'phone_number' => config('admin_credentials.PHONE_NUMBER'),
        ]);

        $user->assignRole(Role::query()->where('name', '=', Roles::OPERATOR->value)->first());
        $user->givePermissionTo(Permission::query()->where('name', '=', Permissions::ADMIN->value)->first());
    }
}
