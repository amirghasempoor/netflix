<?php

namespace Database\Seeders;

use App\Enums\Permissions;
use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Permission::insert([
            [
                'name' => Permissions::MOVIE_MANAGER->value,
                'guard_name' => 'user'
            ],
            [
                'name' => Permissions::USER_MANAGER->value,
                'guard_name' => 'user'
            ],
            [
                'name' => Permissions::ADMIN->value,
                'guard_name' => 'user'
            ],
        ]);
    }
}
