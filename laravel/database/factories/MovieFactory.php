<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Movie>
 */
class MovieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->userName(),
            'description' => fake()->text(),
            'genre' => fake()->name(),
            'publish_day' => fake()->date(),
            'image' => fake()->text(),
        ];
    }
}
