<?php

use App\Http\Controllers\Admin\MovieManagementController;
use App\Http\Controllers\Admin\PermissionManagementController;
use App\Http\Controllers\Admin\RoleManagementController;
use App\Http\Controllers\Admin\UserManagementController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Movie\CategoryController;
use App\Http\Controllers\Movie\SearchController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('auth')
    ->name('auth.')
    ->controller(AuthController::class)
    ->group(function () {
        Route::post('register', 'register')->name('register');
        Route::post('login', 'login')->name('login');
        Route::post('forgot_password', 'forgotPassword')->name('forgotPassword');
        Route::get('/{provider}/redirect', 'redirect')->name('redirect');
        Route::get('/{provider}/callback', 'callback')->name('callback');
        Route::post('logout', 'logout')->middleware('auth')->name('logout');
    }
);

Route::prefix('users')
    ->middleware(['auth', 'role:operator', 'permission:user_manager|admin'])
    ->name('userManagement.')
    ->controller(UserManagementController::class)
    ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'store')->name('store');
        Route::get('/{user}', 'show')->name('show');
        Route::post('/{user}', 'update')->name('update');
        Route::delete('/{user}', 'destroy')->name('delete');
        Route::post('/changePassword/{user}', 'changePassword')->name('changePassword');
    }
);

Route::prefix('movies')
    ->name('movieManagement.')
    ->controller(MovieManagementController::class)
    ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'store')->name('store')->middleware(['auth', 'role:operator', 'permission:movie_manager|admin']);
        Route::get('/{movie}', 'show')->name('show');
        Route::post('/{movie}', 'update')->name('update')->middleware(['auth', 'role:operator', 'permission:movie_manager|admin']);
        Route::post('/change_image/{movie}','changeImage')->name('changeImage')->middleware(['auth', 'role:operator', 'permission:movie_manager|admin']);
        Route::delete('/{movie}', 'destroy')->name('delete')->middleware(['auth', 'role:operator', 'permission:movie_manager|admin']);
    }
);

Route::prefix('roles')
    ->middleware(['auth', 'role:operator', 'permission:admin'])
    ->name('roleManagement.')
    ->controller(RoleManagementController::class)
    ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'store')->name('store');
        Route::get('/{role}', 'show')->name('show');
        Route::post('/{role}', 'update')->name('update');
        Route::delete('/{role}', 'destroy')->name('delete');
    }
);

Route::prefix('permissions')
    ->middleware(['auth', 'role:operator', 'permission:admin'])
    ->name('permissionManagement.')
    ->controller(PermissionManagementController::class)
    ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'store')->name('store');
        Route::get('/{permission}', 'show')->name('show');
        Route::post('/{permission}', 'update')->name('update');
        Route::delete('/{permission}', 'destroy')->name('delete');
    }
);

Route::prefix('profile')
    ->name('profile.')
    ->middleware('auth')
    ->controller(ProfileController::class)
    ->group(function () {
        Route::get('info', 'info')->name('info');
        Route::post('change_password', 'changePassword')->name('changePassword');
        Route::post('change_avatar', 'changeAvatar')->name('changeAvatar');
        Route::post('/add_favorite_movie', 'addFavoriteMovie')->name('addFavoriteMovie');
        Route::post('/delete_favorite_movie', 'deleteFavoriteMovie')->name('deleteFavoriteMovie');
    }
);

Route::get('/category', CategoryController::class)->name('category');
Route::post('/search', SearchController::class)->name('search');
