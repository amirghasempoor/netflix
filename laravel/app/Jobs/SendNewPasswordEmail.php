<?php

namespace App\Jobs;

use App\Mail\ForgotPasswordEmail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SendNewPasswordEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public User $user){}

    /**
     * Execute the job.
     */
    public function handle(): bool
    {
        $password = Str::password(8);

        $this->user->update([
            'password' => Hash::make($password)
        ]);

        Mail::to($this->user->email)->send(new ForgotPasswordEmail($this->user->username, $password));

        return true;
    }
}
