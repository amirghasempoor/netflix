<?php

namespace App\Enums;

enum Permissions: string
{
    case ADMIN = 'admin';
    case MOVIE_MANAGER = 'movie_manager';
    case USER_MANAGER = 'user_manager';
}
