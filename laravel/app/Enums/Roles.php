<?php

namespace App\Enums;

enum Roles: string
{
    case OPERATOR = 'operator';
    case USER = 'user';
}
