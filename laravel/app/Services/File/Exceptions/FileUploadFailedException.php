<?php

namespace App\Services\File\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class FileUploadFailedException extends Exception
{
    /**
     * Report the exception.
     */
    public function report(): void
    {
        Log::channel('file')
            ->error(self::class,
                [
                    __('messages.file_upload_failed'),
                ]
            );
    }

    /**
     * Render the exception into an HTTP response.
     */
    public function render(): JsonResponse
    {
        return response()->json([
            'message' => __('messages.file_upload_failed'),
        ], 500);
    }
}
