<?php

namespace App\Services\File;

use App\Services\File\Exceptions\FileUploadFailedException;
use Illuminate\Http\File as HttpFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileService
{
    /**
     * @throws FileUploadFailedException
     */
    public function save(string $path, HttpFile|UploadedFile $file, string $name, string $disk = 'public'): string
    {
        return
            Storage::disk($disk)->putFileAs($path, $file, \trim($name) . '.' . $file->getClientOriginalExtension()) ?:
            throw new FileUploadFailedException();
    }

    public function delete(string $path, string $disk = 'public'): void
    {
        if (!$path)
        {
            throw new \InvalidArgumentException('The file path should be provided.');
        }

        if (Storage::disk($disk)->exists($path))
        {
            Storage::disk($disk)->delete($path);
        }
    }
}
