<?php

namespace App\Http\Controllers\Movie;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Http\Resources\MovieResource;
use App\Models\Movie;
use Illuminate\Http\JsonResponse;

class SearchController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(SearchRequest $request): JsonResponse
    {
        $searchedMovies = Movie::query()->where('title', 'LIKE', "%{$request->title}%")->get();

        return response()->json(MovieResource::collection($searchedMovies));
    }
}
