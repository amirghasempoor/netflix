<?php

namespace App\Http\Controllers\Movie;

use App\Http\Controllers\Controller;
use App\Http\Resources\MovieResource;
use App\Models\Movie;
use Illuminate\Http\JsonResponse;

class CategoryController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(): JsonResponse
    {
        $genres = ['action', 'drama', 'comedy', 'adventure'];

        $data = [];

        foreach ($genres as $genre) {
            $movies = Movie::query()->where('genre', 'LIKE', '%' . $genre . '%')->get();
            $data[$genre . 'Movies'] = MovieResource::collection($movies);
        }

        return response()->json($data);
    }
}
