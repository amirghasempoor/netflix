<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserManagement\ChangePasswordRequest;
use App\Http\Requests\Admin\UserManagement\StoreRequest;
use App\Http\Requests\Admin\UserManagement\UpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\User;
use App\Services\File\FileService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json(UserResource::collection(User::all()));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request, FileService $file): JsonResponse
    {
        DB::transaction(function () use ($request, $file)
        {
            $avatar = $request->avatar ?
                $file->save('/avatars', $request->avatar, $request->username)
                : null;

            $user = User::create([
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'avatar' => $avatar,
            ]);

            $user->assignRole(Role::find($request->role_id));
        });

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user): JsonResponse
    {
        return response()->json(new UserResource($user));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, User $user): JsonResponse
    {
        DB::transaction(function () use ($request, $user)
        {
            $user->update([
                'username' => $request->username,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
            ]);

            $user->syncRoles($request->role_id);
        });

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user, FileService $file): JsonResponse
    {
        DB::transaction(function () use ($user, $file)
        {
            $user->syncRoles([]);

            if ($user->avatar)
            {
                $file->delete($user->avatar);
            }

            $user->delete();
        });

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    public function changePassword(ChangePasswordRequest $request, User $user): JsonResponse
    {
        $user->update([
            'password' => Hash::make($request->new_password)
        ]);

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }
}
