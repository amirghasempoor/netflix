<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RoleManagement\StoreRequest;
use App\Http\Requests\Admin\RoleManagement\UpdateRequest;
use App\Http\Resources\RoleResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RoleManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json(RoleResource::collection(Role::all()));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): JsonResponse
    {
        DB::transaction(function () use ($request)
        {
            $role = Role::create([
                'name' => $request->name,
            ]);

            $role->syncPermissions($request->permissions);
        });

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role): JsonResponse
    {
        return response()->json(new RoleResource($role));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Role $role): JsonResponse
    {
        DB::transaction(function () use ($request, $role)
        {
            $role->update([
                'name' => $request->name,
            ]);

            $role->syncPermissions($request->permissions);
        });

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role): JsonResponse
    {
        $role->syncPermissions([]);

        $role->delete();

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }
}
