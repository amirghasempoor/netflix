<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PermissionManagement\StoreRequest;
use App\Http\Requests\Admin\PermissionManagement\UpdateRequest;
use App\Http\Resources\PermissionResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json(PermissionResource::collection(Permission::all()));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): JsonResponse
    {
        DB::transaction(function () use ($request)
        {
            Permission::create([
                'name' => $request->name,
            ]);
        });

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Permission $permission): JsonResponse
    {
        return response()->json(new PermissionResource($permission));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Permission $permission): JsonResponse
    {
        DB::transaction(function () use ($request, $permission)
        {
            $permission->update([
                'name' => $request->name,
            ]);
        });

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Permission $permission): JsonResponse
    {
        $permission->delete();

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }
}
