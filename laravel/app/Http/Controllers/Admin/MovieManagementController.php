<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\MovieManagement\ChangeImageRequest;
use App\Http\Requests\Admin\MovieManagement\StoreRequest;
use App\Http\Requests\Admin\MovieManagement\UpdateRequest;
use App\Http\Resources\MovieResource;
use App\Models\Movie;
use App\Services\File\FileService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class MovieManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json(MovieResource::collection(Movie::all()));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request, FileService $file): JsonResponse
    {
        DB::transaction(function () use ($request, $file)
        {
            Movie::create([
                'title' => \ucwords($request->title),
                'description' => $request->description,
                'genre' => $request->genre,
                'publish_day' => $request->publish_day,
                'image' => $file->save('/images', $request->image, $request->title),
            ]);
        });

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Movie $movie): JsonResponse
    {
        return response()->json(new MovieResource($movie));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Movie $movie): JsonResponse
    {
        DB::transaction(function () use ($request, $movie)
        {
            $movie->update([
                'title' => \ucwords($request->title),
                'description' => $request->description,
                'genre' => $request->genre,
                'publish_day' => $request->publish_day,
            ]);
        });

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Movie $movie, FileService $file): JsonResponse
    {
        $file->delete($movie->image);

        $movie->delete();

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    public function changeImage(ChangeImageRequest $request, Movie $movie, FileService $file): JsonResponse
    {
        $file->delete($movie->image);

        $movie->update([
            'image' => $file->save('/images', $request->image, $movie->title),
        ]);

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }
}
