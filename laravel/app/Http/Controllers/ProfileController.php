<?php

namespace App\Http\Controllers;

use App\Http\Requests\Profile\AddFavoriteMovieRequest;
use App\Http\Requests\Profile\ChangeAvatarRequest;
use App\Http\Requests\Profile\ChangePasswordRequest;
use App\Http\Requests\Profile\DeleteFavoriteMovieRequest;
use App\Http\Resources\UserResource;
use App\Models\UserFavoriteMovie;
use App\Services\File\FileService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function info(): JsonResponse
    {
        return response()->json(new UserResource(auth()->user()));
    }

    public function changePassword(ChangePasswordRequest $request): JsonResponse
    {
        $user = auth()->user();

        if (! Hash::check($request->current_password, $user->password))
        {
            return response()->json([
                'message' => __('messages.incorrect_current_password'),
            ], 422);
        }

        $user->update([
            'password' => Hash::make($request->new_password)
        ]);

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    public function changeAvatar(ChangeAvatarRequest $request, FileService $file): JsonResponse
    {
        $user = auth()->user();

        $user->update([
            'avatar' => $file->save('avatars', $request->avatar, $user->username)
        ]);

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    public function addFavoriteMovie(AddFavoriteMovieRequest $request): JsonResponse
    {
        UserFavoriteMovie::create([
            'user_id' => auth()->user()->id,
            'movie_id' => $request->movie_id,
        ]);

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }

    public function deleteFavoriteMovie(DeleteFavoriteMovieRequest $request): JsonResponse
    {
        UserFavoriteMovie::query()->where('user_id', auth()->user()->id)
            ->where('movie_id', $request->movie_id)
            ->delete();

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }
}
