<?php

namespace App\Http\Controllers;

use App\Enums\Roles;
use App\Events\Email\ForgotPassword;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    public function register(RegisterRequest $request): JsonResponse
    {
        $user = User::create([
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'email' => $request->email,
            'phone_number' => $request->phone_number,
        ]);

        $user->assignRole(Role::query()->where('name', '=', Roles::USER->value)->first());

        $token = $user->createToken('USER_TOKEN')->plainTextToken;

        return response()->json([
            'message' => __('messages.successful'),
            'token' => $token
        ]);
    }

    public function login(LoginRequest $request): JsonResponse
    {

        $user = User::query()->where('username', $request->username)->first();

        if (!Hash::check($request->password, $user->password))
        {
            return response()->json([
                'message' => __('messages.invalid_credential')
            ]);
        }

        $token = $user->createToken('USER_TOKEN')->plainTextToken;

        return response()->json([
            'message' => __('messages.successful'),
            'token' => $token
        ]);
    }

    public function forgotPassword(ForgotPasswordRequest $request): JsonResponse
    {
        $user = User::query()->where('username', $request->username)->first();

        ForgotPassword::dispatch($user);

        return response()->json([
            'message' => __('messages.new_password_email_sent'),
        ]);
    }

    public function redirect(string $provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function callback(string $provider): JsonResponse
    {
        $socialiteUser = Socialite::driver($provider)->stateless()->user();

        $username = $socialiteUser->name ?? $socialiteUser->nickname;

        $user = User::updateOrCreate([
            'provider' => $provider,
            'provider_id' => $socialiteUser->id,
        ], [
            'username' => $username,
            'email' => $socialiteUser->email,
            'avatar' => $socialiteUser->avatar,
            'provider_token' => $socialiteUser->token,
            'provider_refresh_token' => $socialiteUser->refreshToken,
        ]);

        if ($user->wasRecentlyCreated){
            $user->assignRole(Role::query()->where('name', '=', Roles::USER->value)->first());
        }

        $token = $user->createToken('USER_TOKEN')->plainTextToken;

        return response()->json([
            'message' => __('messages.successful'),
            'token' => $token
        ]);
    }

    public function logout(): JsonResponse
    {
        auth()->user()->currentAccessToken()->delete();

        return response()->json([
            'message' => __('messages.successful')
        ]);
    }
}
