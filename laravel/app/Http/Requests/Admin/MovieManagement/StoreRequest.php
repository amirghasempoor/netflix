<?php

namespace App\Http\Requests\Admin\MovieManagement;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|unique:movies,title',
            'description' => 'required|string',
            'genre' => 'string',
            'publish_day' => 'date_format:Y-m-d',
            'image' => 'required|mimes:png,jpg,jpeg|max:2048',
        ];
    }
}
