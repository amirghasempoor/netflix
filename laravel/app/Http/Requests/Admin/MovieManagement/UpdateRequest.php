<?php

namespace App\Http\Requests\Admin\MovieManagement;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', Rule::unique('movies', 'title')->ignore($this->movie->id)],
            'description' => 'required',
            'genre' => 'string',
            'publish_day' => 'date_format:Y-m-d',
        ];
    }
}
