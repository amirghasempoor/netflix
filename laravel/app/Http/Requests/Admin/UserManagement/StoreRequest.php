<?php

namespace App\Http\Requests\Admin\UserManagement;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'username' => 'required|string|unique:users,username',
            'password' => 'required|string|confirmed|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/|min:8',
            'email' => 'email|unique:users,email',
            'phone_number' => 'regex:/^09\d{9}$/|numeric|digits:11',
            'avatar' => 'mimes:png,jpg,jpeg|max:2048',
            'role_id' => 'required|exists:roles,id',
        ];
    }
}
