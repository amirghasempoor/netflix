<?php

namespace App\Http\Requests\Admin\UserManagement;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'username' => ['required', 'string', Rule::unique('users', 'username')->ignore($this->user->id)],
            'email' => ['email', Rule::unique('users', 'email')->ignore($this->user->id)],
            'phone_number' => 'regex:/^09\d{9}$/|numeric|digits:11',
            'role_id' => 'required|exists:roles,id',
        ];
    }
}
