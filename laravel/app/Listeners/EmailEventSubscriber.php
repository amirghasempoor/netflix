<?php

namespace App\Listeners;

use App\Events\Email\ForgotPassword;
use App\Jobs\SendNewPasswordEmail;
use Illuminate\Events\Dispatcher;

class EmailEventSubscriber
{
    /**
     * Handle the forgot password event.
     */
    public function handleForgotPasswordEmail(ForgotPassword $event): bool
    {
        SendNewPasswordEmail::dispatch($event->user);

        return true;
    }

    /**
     * Register the listeners for the subscriber.
     */
    public function subscribe(Dispatcher $events): void
    {
        $events->listen(
            ForgotPassword::class,
            [EmailEventSubscriber::class, 'handleForgotPasswordEmail']
        );
    }
}
