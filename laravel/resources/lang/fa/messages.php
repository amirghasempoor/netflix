<?php

return [
    'successful' => 'عملیات با موفقیت انجام شد.',
    'invalid_credential' => 'نام کاربری یا رمز عبور اشتباه است',
    'incorrect_current_password' => 'رمز عبور فعلی اشتباه است',
    'new_password_email_sent' => 'رمز عبور جدید به ایمیل شما ارسال شد.',
    'file_upload_failed' => 'بارگذاری فایل با خطا مواجه شد.',
];
