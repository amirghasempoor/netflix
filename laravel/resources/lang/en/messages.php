<?php

return [
    'successful' => 'The operation was successful.',
    'invalid_credential' => 'Provided credential is invalid.',
    'incorrect_current_password' => 'Current password is incorrect.',
    'new_password_email_sent' => 'Your new password sent to your email',
    'file_upload_failed' => 'The file can not be uploaded.',
];
