<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Password Email</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100 h-screen flex items-center justify-center">

<div class="bg-white p-8 rounded shadow-md max-w-md mx-auto">

    <h1 class="text-2xl font-bold mb-4">Hello, {{ $username }}!</h1>

    <p class="mb-4">Your new password is:</p>

    <div class="bg-gray-200 p-4 rounded">
        <code class="text-lg font-bold">{{ $password }}</code>
    </div>

</div>

</body>
</html>
