<?php

namespace Tests\Feature\Jobs;

use App\Jobs\SendNewPasswordEmail;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class SendNewPasswordEmailTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_send_email_Job_worked_properly(): void
    {
        $user = User::factory()->create();

        Mail::shouldReceive("to->send");

        $job = new SendNewPasswordEmail($user);

        $returnValue = $job->handle();

        $this->assertSame(true, $returnValue);
    }
}
