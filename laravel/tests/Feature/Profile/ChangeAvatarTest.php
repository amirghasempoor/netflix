<?php

namespace Tests\Feature\Profile;

use App\Models\User;
use App\Services\File\FileService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Mockery\MockInterface;
use Tests\TestCase;

class ChangeAvatarTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_avatar_should_match_the_mimes()
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.txt', 40);

        $response = $this->postJson(route('profile.changeAvatar'), [
            'avatar' => $image
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'avatar' => __('validation.mimes', ['attribute' => 'avatar', 'values' => 'png, jpg, jpeg'])
            ]);
    }

    public function test_avatar_should_be_2048kb()
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.jpg', 40000);

        $response = $this->postJson(route('profile.changeAvatar'), [
            'avatar' => $image
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'avatar' => __('validation.max.file', ['attribute' => 'avatar', 'max' => 2048])
            ]);
    }

    public function test_can_change_the_avatar_of_the_user(): void
    {
        $this->withoutExceptionHandling();

        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.jpg', 40);

        $this->mock(FileService::class, function (MockInterface $mock) {
            $mock->shouldReceive('save')->andReturn('image.jpg');
        });

        $response = $this->postJson(route('profile.changeAvatar'), [
            'avatar' => $image,
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseHas('users', [
            'username' => $user->username,
            'avatar' => 'image.jpg'
        ]);
    }
}
