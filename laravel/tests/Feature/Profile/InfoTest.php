<?php

namespace Tests\Feature\Profile;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InfoTest extends TestCase
{
    use RefreshDatabase,WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_can_see_the_info_of_the_user(): void
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->get(route('profile.info'));

        $response->assertStatus(200);

        $response->assertJsonCount(7);

        $response->assertExactJson([
            'username' => $user->username,
            'email' => $user->email,
            'phone_number' => $user->phone_number,
            'avatar' => $user->avatar,
            'role' => $user->getRoleNames()->first(),
            'permissions' => $user->getPermissionNames(),
            'favorite_movies' => $user->movies,
        ]);
    }
}
