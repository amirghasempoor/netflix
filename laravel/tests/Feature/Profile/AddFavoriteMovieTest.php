<?php

namespace Tests\Feature\Profile;

use App\Models\Movie;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AddFavoriteMovieTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_movie_id_is_required(): void
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.addFavoriteMovie'));

        $response->assertUnprocessable()
            ->assertInvalid([
            'movie_id' => __('validation.required', ['attribute' => 'movie id'])
            ]);
    }

    public function test_user_can_add_favorite_movie(): void
    {
        $user = User::factory()
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.addFavoriteMovie'), [
            'movie_id' => $movie->id
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseHas('user_favorite_movies', [
            'user_id' => $user->id,
            'movie_id' => $movie->id,
        ]);
    }
}
