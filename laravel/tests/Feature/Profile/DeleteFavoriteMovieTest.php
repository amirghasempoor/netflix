<?php

namespace Tests\Feature\Profile;

use App\Models\Movie;
use App\Models\User;
use App\Models\UserFavoriteMovie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteFavoriteMovieTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_movie_id_is_required(): void
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.deleteFavoriteMovie'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'movie_id' => __('validation.required', ['attribute' => 'movie id'])
            ]);
    }

    public function test_user_can_delete_favorite_movie(): void
    {
        $user = User::factory()
            ->create();

        $movie = Movie::factory()->create();

        UserFavoriteMovie::create([
            'user_id' => $user->id,
            'movie_id' => $movie->id,
        ]);

        $this->actingAs($user);

        $response = $this->postJson(route('profile.deleteFavoriteMovie'), [
            'movie_id' => $movie->id
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseMissing('user_favorite_movies', [
            'user_id' => $user->id,
            'movie_id' => $movie->id,
        ]);
    }
}
