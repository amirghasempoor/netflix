<?php

namespace Tests\Feature\Profile;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ChangePasswordTest extends TestCase
{
    use RefreshDatabase,WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_current_password_is_required()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.changePassword'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'current_password' => __('validation.required', ['attribute' => 'current password'])
            ]);
    }

    public function test_current_password_should_be_string()
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.changePassword'), [
            'current_password' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'current_password' => __('validation.string', ['attribute' => 'current password'])
            ]);
    }

    public function test_current_password_should_match_the_regex()
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.changePassword'), [
            'current_password' => $this->faker->numberBetween(10000000, 99999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'current_password' => __('validation.regex', ['attribute' => 'current password'])
            ]);
    }

    public function test_current_password_should_have_at_least_8_characters()
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.changePassword'), [
            'current_password' => $this->faker->numberBetween(100000, 999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'current_password' => __('validation.min', ['attribute' => 'current password', 'min' => 8])
            ]);
    }

    public function test_new_password_is_required()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.changePassword'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'new_password' => __('validation.required', ['attribute' => 'new password'])
            ]);
    }

    public function test_new_password_should_be_string()
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.changePassword'), [
            'new_password' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'new_password' => __('validation.string', ['attribute' => 'new password'])
            ]);
    }

    public function test_new_password_should_match_the_regex()
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.changePassword'), [
            'new_password' => $this->faker->numberBetween(10000000, 99999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'new_password' => __('validation.regex', ['attribute' => 'new password'])
            ]);
    }

    public function test_new_password_should_have_at_least_8_characters()
    {
        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('profile.changePassword'), [
            'new_password' => $this->faker->numberBetween(100000, 999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'new_password' => __('validation.min', ['attribute' => 'new password', 'min' => 8])
            ]);
    }

    public function test_can_not_change_the_password_of_the_user_with_wrong_current_password(): void
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();

        $this->actingAs($user);

        $password = $this->faker->numerify('#a######');

        $response = $this->post(route('profile.changePassword'), [
            'current_password' => 'fake1234',
            'new_password' => $password,
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => __('messages.incorrect_current_password'),
        ]);
    }

    public function test_can_change_the_password_of_the_user(): void
    {
        $this->withoutExceptionHandling();

        $user = User::factory()
            ->create();

        $this->actingAs($user);

        $response = $this->post(route('profile.changePassword'), [
            'current_password' => 'password1234',
            'new_password' => $this->faker->numerify('#a######'),
        ]);

        $response->assertStatus(200);

        $response->assertExactJson([
            'message' => __('messages.successful')
        ]);
    }
}
