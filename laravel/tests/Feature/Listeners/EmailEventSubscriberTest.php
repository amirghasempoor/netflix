<?php

namespace Tests\Feature\Listeners;

use App\Events\Email\ForgotPassword;
use App\Jobs\SendNewPasswordEmail;
use App\Listeners\EmailEventSubscriber;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class EmailEventSubscriberTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_send_new_password_email_job_queued(): void
    {
        Queue::fake();

        $user = User::factory()->create();

        $listener = new EmailEventSubscriber();

        $returnValue = $listener->handleForgotPasswordEmail(new ForgotPassword($user));

        Queue::assertPushed(SendNewPasswordEmail::class, function ($job) use ($user) {
            return $job->user === $user;
        });
        $this->assertSame(true, $returnValue);
    }
}
