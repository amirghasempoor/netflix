<?php

namespace Tests\Feature\Services;

use App\Services\File\Exceptions\FileUploadFailedException;
use App\Services\File\FileService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class FileTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_can_save_a_file(): void
    {
        Storage::fake('public');

        $inputFile = UploadedFile::fake()->image('image.jpg');

        $path = (new FileService())->save('fakeDir', $inputFile, 'fakeName');

        Storage::disk('public')->assertExists($path);
    }

    public function test_exception_throws_successfully(): void
    {
        Storage::shouldReceive('disk->putFileAs')
            ->andReturn(false);

        $inputFile = UploadedFile::fake()->image($this->faker->image);

        $this->expectException(FileUploadFailedException::class);

        (new FileService())->save('fakeDir', $inputFile, 'fakeName');
    }

    public function test_can_not_delete_with_empty_path(): void
    {
        $this->assertThrows(
            fn() => (new FileService())->delete(''),
            \InvalidArgumentException::class
        );
    }

    public function test_can_delete_a_file(): void
    {
        Storage::fake('public');

        $fileName = 'fake' . '.' . $this->faker->fileExtension();

        $inputFile = UploadedFile::fake()->create($fileName);

        $path = Storage::disk('public')->putFile('testDir', $inputFile);

        Storage::disk('public')->assertExists($path);

        (new FileService())->delete($path);

        Storage::disk('public')->assertMissing($path);
    }
}
