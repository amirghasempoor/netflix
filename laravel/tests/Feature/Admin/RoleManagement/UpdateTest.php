<?php

namespace Tests\Feature\Admin\RoleManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_name_is_required()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $role = Role::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.update', [$role->id]));

        $response->assertUnprocessable()
            ->assertInvalid([
                'name' => __('validation.required', ['attribute' => 'name'])
            ]);
    }

    public function test_name_should_be_unique_in_roles_table()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $role1 = Role::factory()->create();
        $role2 = Role::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.update', [$role1->id]), [
            'name' => $role2->name
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'name' => __('validation.unique', ['attribute' => 'name'])
            ]);
    }

    public function test_permissions_is_required()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $role = Role::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.update', [$role->id]));

        $response->assertUnprocessable()
            ->assertInvalid([
                'permissions' => __('validation.required', ['attribute' => 'permissions'])
            ]);
    }

    public function test_permissions_should_be_existed_in_permissions_table()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $role = Role::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.update', [$role->id]), [
            'permissions' => [$this->faker->numberBetween(1, 9)]
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'permissions' => __('validation.exists', ['attribute' => 'permissions'])
            ]);
    }

    public function test_permissions_should_be_array_format()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $role = Role::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.update', [$role->id]), [
            'permissions' => $this->faker->numberBetween(1, 9)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'permissions' => __('validation.array', ['attribute' => 'permissions'])
            ]);
    }

    public function test_can_update_a_role()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $role = Role::factory()->create();

        $permission = Permission::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.update', [$role->id]), [
            'name' => 'test',
            'permissions' => [$permission->id],
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

            $this->assertDatabaseHas('roles', [
                'name' => 'test',
            ]);

            $this->assertDatabaseHas('role_has_permissions', [
                'permission_id' => $permission->id,
                'role_id' => 2
            ]);
    }
}
