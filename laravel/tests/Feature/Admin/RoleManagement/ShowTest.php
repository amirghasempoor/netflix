<?php

namespace Tests\Feature\Admin\RoleManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_can_see_a_role_detail(): void
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::ADMIN->value
            ]))
            ->create();

        $role = Role::factory()->create();

        $this->actingAs($user);

        $response = $this->getJson(route('roleManagement.show', [$role->id]));

        $response->assertOk();

        $response->assertJsonCount(2);

        $response->assertJsonStructure([
            'name',
            'permissions',
        ]);
    }
}
