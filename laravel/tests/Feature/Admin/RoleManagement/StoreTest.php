<?php

namespace Tests\Feature\Admin\RoleManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StoreTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_name_is_required()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.store'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'name' => __('validation.required', ['attribute' => 'name'])
            ]);
    }

    public function test_name_should_be_unique_in_roles_table()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $role = Role::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.store'), [
            'name' => $role->name
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'name' => __('validation.unique', ['attribute' => 'name'])
            ]);
    }

    public function test_permissions_is_required()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.store'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'permissions' => __('validation.required', ['attribute' => 'permissions'])
            ]);
    }

    public function test_permissions_should_be_existed_in_permissions_table()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.store'), [
            'permissions' => [$this->faker->numberBetween(1, 9)]
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'permissions' => __('validation.exists', ['attribute' => 'permissions'])
            ]);
    }

    public function test_permissions_should_be_array_format()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.store'), [
            'permissions' => $this->faker->numberBetween(1, 9)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'permissions' => __('validation.array', ['attribute' => 'permissions'])
            ]);
    }

    public function test_can_create_a_role()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $permission = Permission::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('roleManagement.store'), [
            'name' => 'role',
            'permissions' => [$permission->id],
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseHas('roles', [
            'name' => 'role',
        ]);

        $this->assertDatabaseHas('role_has_permissions', [
            'permission_id' => $permission->id,
            'role_id' => 2
        ]);
    }
}
