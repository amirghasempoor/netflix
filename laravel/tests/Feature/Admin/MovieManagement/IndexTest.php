<?php

namespace Tests\Feature\Admin\MovieManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Movie;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class IndexTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_can_see_movies(): void
    {
        Movie::factory(10)->create();

        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->getJson(route('movieManagement.index'));

        $response->assertOk();

        $response->assertJsonCount(10);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'title',
                'description',
                'publish_day',
                'genre',
                'image',
            ],
        ]);
    }
}
