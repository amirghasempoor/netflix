<?php

namespace Tests\Feature\Admin\MovieManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Movie;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Services\File\FileService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Mockery\MockInterface;
use Tests\TestCase;

class ChangImageTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_image_is_required()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.changeImage', $movie->id));

        $response->assertUnprocessable()
            ->assertInvalid([
                'image' => __('validation.required', ['attribute' => 'image'])
            ]);
    }

    public function test_image_should_match_the_mimes()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.txt', 40);

        $response = $this->postJson(route('movieManagement.changeImage', $movie->id), [
            'image' => $image
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'image' => __('validation.mimes', ['attribute' => 'image', 'values' => 'png, jpg, jpeg'])
            ]);
    }

    public function test_image_should_be_2048kb()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.jpg', 40000);

        $response = $this->postJson(route('movieManagement.changeImage', $movie->id), [
            'image' => $image
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'image' => __('validation.max.file', ['attribute' => 'image', 'max' => 2048])
            ]);
    }

    public function test_can_change_image_of_a_movie()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.jpg', 40);

        $this->mock(FileService::class, function (MockInterface $mock) {
            $mock->shouldReceive('delete', 'save')->andReturn('image.jpg');
        });

        $response = $this->postJson(route('movieManagement.changeImage', $movie->id), [
            'image' => $image
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseHas('movies', [
            'title' => $movie->title,
            'description' => $movie->description,
            'genre' => $movie->genre,
            'image' => 'image.jpg'
        ]);
    }
}
