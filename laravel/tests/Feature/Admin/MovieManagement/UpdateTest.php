<?php

namespace Tests\Feature\Admin\MovieManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Facades\File\FileFacade;
use App\Models\Movie;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_title_is_required(): void
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.update', [$movie->id]));

        $response->assertUnprocessable()
            ->assertInvalid([
                'title' => __('validation.required', ['attribute' => 'title'])
            ]);
    }

    public function test_title_should_be_unique(): void
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie1 = Movie::factory()->create();
        $movie2 = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.update', [$movie1->id]), [
            'title' => $movie2->title
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'title' => __('validation.unique', ['attribute' => 'title'])
            ]);
    }

    public function test_description_is_required(): void
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.update', [$movie->id]));

        $response->assertUnprocessable()
            ->assertInvalid([
                'description' => __('validation.required', ['attribute' => 'description'])
            ]);
    }

    public function test_genre_should_be_string(): void
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.update', [$movie->id]), [
            'genre' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'genre' => __('validation.string', ['attribute' => 'genre'])
            ]);
    }

    public function test_publish_day_should_match_the_format()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.update', [$movie->id]), [
            'publish_day' => $this->faker->date('d-m-Y')
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'publish_day' => __('validation.date_format', ['attribute' => 'publish day', 'format' => 'Y-m-d'])
            ]);
    }

    public function test_can_update_a_movie()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.update', [$movie->id]), [
            'title' => 'Title',
            'description' => 'description',
            'genre' => 'comedy',
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseHas('movies', [
            'title' => 'Title',
            'description' => 'description',
            'genre' => 'comedy',
        ]);
    }
}
