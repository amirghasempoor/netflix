<?php

namespace Tests\Feature\Admin\MovieManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Movie;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Services\File\FileService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Mockery\MockInterface;
use Tests\TestCase;

class StoreTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_title_is_required()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.store'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'title' => __('validation.required', ['attribute' => 'title'])
            ]);
    }

    public function test_title_should_be_string()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.store'), [
            'title' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'title' => __('validation.string', ['attribute' => 'title'])
            ]);
    }

    public function test_title_should_be_unique_in_movies_table()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.store'), [
            'title' => $movie->title
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'title' => __('validation.unique', ['attribute' => 'title'])
            ]);
    }

    public function test_description_is_required()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.store'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'description' => __('validation.required', ['attribute' => 'description'])
            ]);
    }

    public function test_description_should_be_string()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.store'), [
            'description' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'description' => __('validation.string', ['attribute' => 'description'])
            ]);
    }

    public function test_genre_should_be_string()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.store'), [
            'genre' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'genre' => __('validation.string', ['attribute' => 'genre'])
            ]);
    }

    public function test_publish_day_should_match_the_format()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.store'), [
            'publish_day' => $this->faker->date('d-m-Y')
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'publish_day' => __('validation.date_format', ['attribute' => 'publish day', 'format' => 'Y-m-d'])
            ]);
    }

    public function test_image_is_required()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('movieManagement.store'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'image' => __('validation.required', ['attribute' => 'image'])
            ]);
    }

    public function test_image_should_match_the_mimes()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.txt', 40);

        $response = $this->postJson(route('movieManagement.store'), [
            'image' => $image
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'image' => __('validation.mimes', ['attribute' => 'image', 'values' => 'png, jpg, jpeg'])
            ]);
    }

    public function test_image_should_be_2048kb()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.jpg', 40000);

        $response = $this->postJson(route('movieManagement.store'), [
            'image' => $image
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'image' => __('validation.max.file', ['attribute' => 'image', 'max' => 2048])
            ]);
    }

    public function test_can_create_a_movie()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.jpg', 40);

        $this->mock(FileService::class, function (MockInterface $mock) {
            $mock->shouldReceive('save')->andReturn('image.jpg');
        });

        $response = $this->postJson(route('movieManagement.store'), [
            'title' => 'Title',
            'description' => 'description',
            'genre' => 'comedy',
            'image' => $image
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseHas('movies', [
            'title' => 'Title',
            'description' => 'description',
            'genre' => 'comedy',
        ]);
    }
}
