<?php

namespace Tests\Feature\Admin\MovieManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Movie;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Services\File\FileService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery\MockInterface;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_can_delete_a_movie(): void
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $this->mock(FileService::class, function (MockInterface $mock) {
            $mock->shouldReceive('delete');
        });

        $response = $this->delete(route('movieManagement.delete', [$movie->id]));

        $response->assertOk();

        $this->assertDatabaseMissing('movies', [
            'id' => $movie->id,
            'title' => $movie->title,
            'description' => $movie->description,
            'genre' => $movie->genre,
            'publish_day' => $movie->publish_day,
            'image' => $movie->image,
        ]);
    }
}
