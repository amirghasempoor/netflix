<?php

namespace Tests\Feature\Admin\MovieManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Movie;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_can_see_a_movie_detail(): void
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::MOVIE_MANAGER->value
            ]))
            ->create();

        $movie = Movie::factory()->create();

        $this->actingAs($user);

        $response = $this->getJson(route('movieManagement.show', [$movie->id]));

        $response->assertOk();

        $response->assertJsonCount(6);

        $response->assertJsonStructure([
            'title',
            'description',
            'genre',
            'publish_day',
            'image',
        ]);
    }
}
