<?php

namespace Tests\Feature\Admin\PermissionManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class IndexTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_can_see_permissions(): void
    {
        Permission::factory(10)->create();

        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::ADMIN->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->getJson(route('permissionManagement.index'));

        $response->assertOk();

        $response->assertJsonCount(11);

        $response->assertJsonStructure([
            '*' => [
                'name',
            ],
        ]);
    }
}
