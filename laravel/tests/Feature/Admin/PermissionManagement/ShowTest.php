<?php

namespace Tests\Feature\Admin\PermissionManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_can_see_a_permission_detail(): void
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::ADMIN->value
            ]))
            ->create();

        $permission = Permission::factory()->create();

        $this->actingAs($user);

        $response = $this->getJson(route('permissionManagement.show', [$permission->id]));

        $response->assertOk();

        $response->assertJsonCount(1);

        $response->assertJsonStructure([
            'name',
        ]);
    }
}
