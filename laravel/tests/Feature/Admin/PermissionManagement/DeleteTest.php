<?php

namespace Tests\Feature\Admin\PermissionManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
     public function test_can_delete_a_permission(): void
     {
         $this->withoutExceptionHandling();

         $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

         $permission = Permission::factory()->create();

         Sanctum::actingAs($user);

         $response = $this->delete(route('permissionManagement.delete', [$permission->id]));

         $response->assertOk();

         $this->assertDatabaseMissing('permissions', [
             'name' => $permission->name,
         ]);
     }
}
