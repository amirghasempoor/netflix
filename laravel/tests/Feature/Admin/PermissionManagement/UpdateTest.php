<?php

namespace Tests\Feature\Admin\PermissionManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_name_is_required()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $permission = Permission::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('permissionManagement.update', [$permission->id]));

        $response->assertUnprocessable()
            ->assertInvalid([
                'name' => __('validation.required', ['attribute' => 'name'])
            ]);
    }

    public function test_name_should_be_unique_in_permissions_table()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $permission1 = Permission::factory()->create();
        $permission2 = Permission::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('permissionManagement.update', [$permission1->id]), [
            'name' => $permission2->name
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'name' => __('validation.unique', ['attribute' => 'name'])
            ]);
    }

    public function test_can_update_a_permission()
    {
        $user = User::factory()
             ->has(Role::factory([
                 'name' => Roles::OPERATOR->value
             ]))
             ->has(Permission::factory([
                 'name' => Permissions::ADMIN->value
             ]))
             ->create();

        $permission = Permission::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('permissionManagement.update', [$permission->id]), [
            'name' => 'permission',
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseHas('permissions', [
            'name' => 'permission',
        ]);
    }
}
