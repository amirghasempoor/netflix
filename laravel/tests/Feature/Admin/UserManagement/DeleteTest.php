<?php

namespace Tests\Feature\Admin\UserManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Services\File\FileService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery\MockInterface;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_can_delete_a_user(): void
    {
        $user1 = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $user2 = User::factory()->create();

        $this->actingAs($user1);

        $this->mock(FileService::class, function (MockInterface $mock) {
            $mock->shouldReceive('delete');
        });

        $response = $this->delete(route('userManagement.delete', [$user2->id]));

        $response->assertOk();

        $this->assertDatabaseMissing('users', [
            'id' => $user2->id,
            'username' => $user2->username,
            'email' => $user2->email,
            'phone_number' => $user2->phone_number,
        ]);
    }
}
