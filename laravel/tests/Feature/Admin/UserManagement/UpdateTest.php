<?php

namespace Tests\Feature\Admin\UserManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_username_is_required(): void
    {
        User::factory()->create([
            'id' => 100
        ]);

        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.update', [100]));

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.required', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_string(): void
    {
        User::factory()->create([
            'id' => 100
        ]);

        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.update', [100]), [
            'username' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.string', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_unique(): void
    {
        $user2 = User::factory()->create([
            'id' => 100
        ]);

        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.update', [$user->id]), [
            'username' => $user2->username
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.unique', ['attribute' => 'username'])
            ]);
    }

    public function test_email_should_be_valid_email_address()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.update', [$user->id]), [
            'email' => $this->faker->numberBetween(100000, 999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'email' => __('validation.email', ['attribute' => 'email'])
            ]);
    }

    public function test_email_should_be_unique_in_users_table()
    {

        $user1 = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $user2 = User::factory()->create([
            'id' => 100
        ]);

        $this->actingAs($user1);

        $response = $this->postJson(route('userManagement.update', [$user2->id]), [
            'email' => $user1->email
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'email' => __('validation.unique', ['attribute' => 'email'])
            ]);
    }

    public function test_phone_number_should_match_the_regex()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.update', [$user->id]), [
            'phone_number' => $this->faker->numberBetween(1000000, 9999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'phone_number' => __('validation.regex', ['attribute' => 'phone number'])
            ]);
    }

    public function test_phone_number_should_be_numeric()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.update', [$user->id]), [
            'phone_number' => $this->faker->name()
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'phone_number' => __('validation.numeric', ['attribute' => 'phone number'])
            ]);
    }

    public function test_phone_number_should_be_11_digits()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.update', [$user->id]), [
            'phone_number' => $this->faker->numberBetween(10000, 99999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'phone_number' => __('validation.digits', ['attribute' => 'phone number', 'digits' => 11])
            ]);
    }

    public function test_can_update_a_user()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $role = Role::factory()->create();

        $this->actingAs($user);

        $password = $this->faker->numerify('#a######');

        $response = $this->postJson(route('userManagement.update', [$user->id]), [
            'username' => 'testUsername',
            'password' => $password,
            'password_confirmation' => $password,
            'email' => 'test@gmail.com',
            'role_id' => $role->id
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseHas('users', [
            'username' => 'testUsername',
            'email' => 'test@gmail.com',
        ]);

        $this->assertDatabaseHas('model_has_roles', [
            'role_id' => $role->id,
            'model_id' => $user->id,
            'model_type' => User::class
        ]);
    }
}
