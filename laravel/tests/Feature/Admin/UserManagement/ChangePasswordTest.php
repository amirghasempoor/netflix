<?php

namespace Tests\Feature\Admin\UserManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChangePasswordTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_new_password_is_required()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.changePassword', [$user->id]));

        $response->assertUnprocessable()
            ->assertInvalid([
                'new_password' => __('validation.required', ['attribute' => 'new password'])
            ]);
    }

    public function test_new_password_should_be_string()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.changePassword', [$user->id]), [
            'new_password' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'new_password' => __('validation.string', ['attribute' => 'new password'])
            ]);
    }

    public function test_new_password_should_match_the_regex()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.changePassword', [$user->id]), [
            'new_password' => $this->faker->numberBetween(10000000, 99999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'new_password' => __('validation.regex', ['attribute' => 'new password'])
            ]);
    }

    public function test_new_password_should_have_at_least_8_characters()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.changePassword', [$user->id]), [
            'new_password' => $this->faker->numberBetween(100000, 999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'new_password' => __('validation.min', ['attribute' => 'new password', 'min' => 8])
            ]);
    }

    public function test_can_change_the_password_of_the_user(): void
    {
        $this->withoutExceptionHandling();

        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $password = $this->faker->numerify('#a######');

        $response = $this->post(route('userManagement.changePassword', [$user->id]), [
            'current_password' => $password,
            'new_password' => $password,
        ]);

        $response->assertStatus(200);

        $response->assertExactJson([
            'message' => __('messages.successful')
        ]);
    }
}
