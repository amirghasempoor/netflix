<?php

namespace Tests\Feature\Admin\UserManagement;

use App\Enums\Permissions;
use App\Enums\Roles;
use App\Facades\File\FileFacade;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Services\File\FileService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Mockery\MockInterface;
use Tests\TestCase;

class StoreTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_username_is_required()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.required', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_string()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'username' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.string', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_unique_in_users_table()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'username' => $user->username
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.unique', ['attribute' => 'username'])
            ]);
    }

    public function test_password_is_required()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.required', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_be_string()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'password' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.string', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_be_confirmed()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'password' => $this->faker->numerify('########')
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.confirmed', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_match_the_regex()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'password' => $this->faker->numberBetween(10000000, 99999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.regex', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_have_at_least_8_characters()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'password' => $this->faker->numberBetween(100000, 999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.min', ['attribute' => 'password', 'min' => 8])
            ]);
    }

    public function test_email_should_be_valid_email_address()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'email' => $this->faker->numberBetween(100000, 999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'email' => __('validation.email', ['attribute' => 'email'])
            ]);
    }

    public function test_email_should_be_unique_in_users_table()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'email' => $user->email
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'email' => __('validation.unique', ['attribute' => 'email'])
            ]);
    }

    public function test_phone_number_should_match_the_regex()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'phone_number' => $this->faker->numberBetween(1000000, 9999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'phone_number' => __('validation.regex', ['attribute' => 'phone number'])
            ]);
    }

    public function test_phone_number_should_be_numeric()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'phone_number' => $this->faker->name()
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'phone_number' => __('validation.numeric', ['attribute' => 'phone number'])
            ]);
    }

    public function test_phone_number_should_be_11_digits()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'phone_number' => $this->faker->numberBetween(10000, 99999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'phone_number' => __('validation.digits', ['attribute' => 'phone number', 'digits' => 11])
            ]);
    }

    public function test_avatar_should_match_the_mimes()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.txt', 40);

        $response = $this->postJson(route('userManagement.store'), [
            'avatar' => $image
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'avatar' => __('validation.mimes', ['attribute' => 'avatar', 'values' => 'png, jpg, jpeg'])
            ]);
    }

    public function test_avatar_should_be_2048kb()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.jpg', 40000);

        $response = $this->postJson(route('userManagement.store'), [
            'avatar' => $image
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'avatar' => __('validation.max.file', ['attribute' => 'avatar', 'max' => 2048])
            ]);
    }

    public function test_role_is_required()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'role_id' => __('validation.required', ['attribute' => 'role id'])
            ]);
    }

    public function test_role_should_be_existed_in_roles_table()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $this->actingAs($user);

        $response = $this->postJson(route('userManagement.store'), [
            'role_id' => [$this->faker->numberBetween(10, 90)]
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'role_id' => __('validation.exists', ['attribute' => 'role id'])
            ]);
    }

    public function test_can_create_a_user()
    {
        $user = User::factory()
            ->has(Role::factory([
                'name' => Roles::OPERATOR->value
            ]))
            ->has(Permission::factory([
                'name' => Permissions::USER_MANAGER->value
            ]))
            ->create();

        $role = Role::factory()->create();

        $this->actingAs($user);

        $image = UploadedFile::fake()->create('image.jpg', 40);

        $password = $this->faker->numerify('#a######');

        $this->mock(FileService::class, function (MockInterface $mock) {
            $mock->shouldReceive('save')->andReturn('image.jpg');
        });

        $response = $this->postJson(route('userManagement.store'), [
            'username' => 'testUsername',
            'password' => $password,
            'password_confirmation' => $password,
            'email' => 'test@gmail.com',
            'avatar' => $image,
            'role_id' => $role->id
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful')
            ]);

        $this->assertDatabaseHas('users', [
            'username' => 'testUsername',
            'email' => 'test@gmail.com',
            'avatar' => 'image.jpg'
        ]);

        $this->assertDatabaseHas('model_has_roles', [
            'role_id' => $role->id,
            'model_id' => 2,
            'model_type' => User::class
        ]);
    }
}
