<?php

namespace Tests\Feature\Movie;

use App\Models\Movie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_can_see_categories(): void
    {
        Movie::factory()
            ->sequence(
                ['genre' => 'action, comdey'],
                ['genre' => 'drama, adventure'],
                ['genre' => 'action'],
                ['genre' => 'comdey'],
                ['genre' => 'drama, comdey'],
                ['genre' => 'action, adventure'],
                ['genre' => 'adventure, comdey'],
                ['genre' => 'action, drama'],
            )
            ->count(8)
            ->create();


        $response = $this->get(route('category'));

        $response->assertOk();

        $response->assertJsonCount(4);

        $response->assertJsonStructure([
            'actionMovies' => [
                '*' => [
                    "id",
                    "title",
                    "description",
                    "publish_day",
                    "genre",
                    "image"
                ]
            ],
            'dramaMovies' => [
                '*' => [
                    "id",
                    "title",
                    "description",
                    "publish_day",
                    "genre",
                    "image"
                ]
            ],
            'comedyMovies' => [
                '*' => [
                    "id",
                    "title",
                    "description",
                    "publish_day",
                    "genre",
                    "image"
                ]
            ],
            'adventureMovies' => [
                '*' => [
                    "id",
                    "title",
                    "description",
                    "publish_day",
                    "genre",
                    "image"
                ]
            ],
        ]);
    }
}
