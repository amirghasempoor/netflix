<?php

namespace Tests\Feature\Movie;

use App\Models\Movie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_title_is_required()
    {
        $response = $this->postJson(route('search'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'title' => __('validation.required', ['attribute' => 'title'])
            ]);
    }

    public function test_can_search_for_a_movie()
    {
        Movie::factory()->create();

        $response = $this->postJson(route('search'), [
            'title' => $this->faker->lexify('?')
        ]);

        $response->assertOk()
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'title',
                    'description',
                    'publish_day',
                    'genre',
                    'image'
                ]
            ]);
    }
}
