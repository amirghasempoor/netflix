<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\PersonalAccessToken;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_username_is_required()
    {
        $response = $this->postJson(route('auth.login'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.required', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_string()
    {
        $response = $this->postJson(route('auth.login'), [
            'username' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.string', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_existed_in_users_table()
    {
        $response = $this->postJson(route('auth.login'), [
            'username' => $this->faker->name
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.exists', ['attribute' => 'username'])
            ]);
    }

    public function test_password_is_required()
    {
        $response = $this->postJson(route('auth.login'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.required', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_be_string()
    {
        $response = $this->postJson(route('auth.login'), [
            'password' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.string', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_match_the_regex()
    {
        $response = $this->postJson(route('auth.login'), [
            'password' => $this->faker->numberBetween(10000000, 99999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.regex', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_have_at_least_8_characters()
    {
        $response = $this->postJson(route('auth.login'), [
            'password' => $this->faker->numberBetween(100000, 999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.min', ['attribute' => 'password', 'min' => 8])
            ]);
    }

    public function test_a_user_can_log_in()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();


        $response = $this->postJson(route('auth.login'), [
            'username' => $user->username,
            'password' => 'password1234'
        ]);

        $response->assertOk()
                ->assertExactJson([
                    'message' => __('messages.successful'),
                    'token' => $response->original["token"]
                ]);

        $token = PersonalAccessToken::findToken($response->original["token"]);

        $this->assertInstanceOf(PersonalAccessToken::class, $token); // test if the token is existed in personal access token table

        $this->assertTrue($token->tokenable_type == User::class); // test if the generated token is assign to the right model

        $this->assertTrue($token->tokenable_id == $user->id); // test if the generated token is assign to the right id
    }

    public function test_user_can_not_log_in_with_wrong_password()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();

        $response = $this->postJson(route('auth.login'), [
            'username' => $user->username,
            'password' => '12345678pass'
        ]);

        $response->assertExactJson([
            'message' => __('messages.invalid_credential'),
        ]);
    }
}
