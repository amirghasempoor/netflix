<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use App\Events\Email\ForgotPassword as ForgotPasswordEvent;
use Tests\TestCase;

class ForgotPassword extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_username_is_required()
    {
        $response = $this->postJson(route('auth.forgotPassword'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.required', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_string()
    {
        $response = $this->postJson(route('auth.forgotPassword'), [
            'username' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.string', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_existed_in_users_table()
    {
        $response = $this->postJson(route('auth.forgotPassword'), [
            'username' => $this->faker->name
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.exists', ['attribute' => 'username'])
            ]);
    }

    public function test_forgot_password_email_sent_successfully()
    {
        $user = User::factory()->create();

        Event::fake();

        $response = $this->postJson(route('auth.forgotPassword'), [
            'username' => $user->username
        ]);

        Event::assertDispatched(ForgotPasswordEvent::class);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.new_password_email_sent'),
            ]);
    }
}
