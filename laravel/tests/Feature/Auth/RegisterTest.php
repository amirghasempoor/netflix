<?php

namespace Tests\Feature\Auth;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\PersonalAccessToken;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_username_is_required()
    {
        $response = $this->postJson(route('auth.register'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.required', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_string()
    {
        $response = $this->postJson(route('auth.register'), [
            'username' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.string', ['attribute' => 'username'])
            ]);
    }

    public function test_username_should_be_unique_in_users_table()
    {
        $user = User::factory()->create();

        $response = $this->postJson(route('auth.register'), [
            'username' => $user->username
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'username' => __('validation.unique', ['attribute' => 'username'])
            ]);
    }

    public function test_password_is_required()
    {
        $response = $this->postJson(route('auth.register'));

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.required', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_be_string()
    {
        $response = $this->postJson(route('auth.register'), [
            'password' => $this->faker->numberBetween(1, 10)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.string', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_be_confirmed()
    {
        $response = $this->postJson(route('auth.register'), [
            'password' => $this->faker->numerify('########')
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.confirmed', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_match_the_regex()
    {
        $response = $this->postJson(route('auth.register'), [
            'password' => $this->faker->numberBetween(10000000, 99999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.regex', ['attribute' => 'password'])
            ]);
    }

    public function test_password_should_have_at_least_8_characters()
    {
        $response = $this->postJson(route('auth.register'), [
            'password' => $this->faker->numberBetween(100000, 999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'password' => __('validation.min', ['attribute' => 'password', 'min' => 8])
            ]);
    }

    public function test_email_should_be_valid_email_address()
    {
        $response = $this->postJson(route('auth.register'), [
            'email' => $this->faker->numberBetween(100000, 999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'email' => __('validation.email', ['attribute' => 'email'])
            ]);
    }

    public function test_email_should_be_unique_in_users_table()
    {
        $user = User::factory()->create();

        $response = $this->postJson(route('auth.register'), [
            'email' => $user->email
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'email' => __('validation.unique', ['attribute' => 'email'])
            ]);
    }

    public function test_phone_number_should_match_the_regex()
    {
        $response = $this->postJson(route('auth.register'), [
            'phone_number' => $this->faker->numberBetween(1000000, 9999999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'phone_number' => __('validation.regex', ['attribute' => 'phone number'])
            ]);
    }

    public function test_phone_number_should_be_numeric()
    {
        $response = $this->postJson(route('auth.register'), [
            'phone_number' => $this->faker->name()
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'phone_number' => __('validation.numeric', ['attribute' => 'phone number'])
            ]);
    }

    public function test_phone_number_should_be_11_digits()
    {
        $response = $this->postJson(route('auth.register'), [
            'phone_number' => $this->faker->numberBetween(10000, 99999)
        ]);

        $response->assertUnprocessable()
            ->assertInvalid([
                'phone_number' => __('validation.digits', ['attribute' => 'phone number', 'digits' => 11])
            ]);
    }

    public function test_can_register_a_user()
    {
        $password = $this->faker->numerify('#a######');

        $role = Role::factory()->create([
            'name' => 'user'
        ]);

        $response = $this->postJson(route('auth.register'), [
            'username' => 'testUsername',
            'password' => $password,
            'password_confirmation' => $password,
            'email' => 'test@gmail.com',
        ]);

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful'),
                'token' => $response->original["token"]
            ]);

        $this->assertDatabaseHas('users', [
            'username' => 'testUsername',
            'email' => 'test@gmail.com',
        ]);

        $this->assertDatabaseHas('model_has_roles', [
            'role_id' => $role->id,
            'model_id' => 1,
            'model_type' => User::class
        ]);

        $token = PersonalAccessToken::findToken($response->original["token"]);

        $this->assertInstanceOf(PersonalAccessToken::class, $token); // test if the token is existed in personal access token table

        $this->assertTrue($token->tokenable_type == User::class); // test if the generated token is assign to the right model
    }
}
