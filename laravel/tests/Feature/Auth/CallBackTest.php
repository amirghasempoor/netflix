<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\PersonalAccessToken;
use Laravel\Socialite\Facades\Socialite;
use Tests\TestCase;

class CallBackTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_can_authenticate_with_a_provider(): void
    {
        $this->withoutExceptionHandling();

        $socialiteData = [
            'id' => $this->faker->numberBetween(1, 10),
            'name' => $this->faker->name(),
            'email' => $this->faker->email(),
            'refreshToken' => $this->faker->text(),
            'token' => $this->faker->text(),
            'avatar' => $this->faker->lexify('??????'),
            'provider' => 'github',
            'provider_id' => $this->faker->numberBetween(1, 10),
        ];

        Socialite::shouldReceive('driver->stateless->user')
            ->andReturn((object) $socialiteData);

        $response = $this->get(route('auth.callback', 'github'));

        $response->assertOk()
            ->assertExactJson([
                'message' => __('messages.successful'),
                'token' => $response->original["token"]
            ]);

        $token = PersonalAccessToken::findToken($response->original["token"]);

        $this->assertInstanceOf(PersonalAccessToken::class, $token);

        $this->assertTrue($token->tokenable_type == User::class);

        $this->assertDatabaseHas('users', [
            'username' => $socialiteData['name'],
            'email' => $socialiteData['email'],
            'provider' => $socialiteData['provider'],
            'provider_refresh_token' => $socialiteData['refreshToken'],
            'provider_id' => $socialiteData['id'],
            'provider_token' => $socialiteData['token'],
        ]);
    }
}
