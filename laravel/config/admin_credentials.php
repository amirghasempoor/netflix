<?php

return [
    'USERNAME' => env('USERNAME'),
    'PASSWORD' => env('PASSWORD'),
    'EMAIL' => env('EMAIL'),
    'PHONE_NUMBER' => env('PHONE_NUMBER'),
];
